FROM node:17.2.0-bullseye
WORKDIR /home/node/app
COPY . .
RUN npm install
EXPOSE 8000
CMD npm start

